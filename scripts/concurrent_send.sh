#!/bin/bash

# Define the base URL for the store endpoint
STORE_URL="http://localhost:8080/store"

# Function to send data for a given file_id
send_data() {
  local file_id="$1"
  
  # Generate a larger JSON payload with a unique file_id and 2KB of data
  # Adjust the size of the data field to ensure the payload is approximately 2KB
  JSON_DATA=$(jq -n --arg file_id "$file_id" --arg data "$(head -c 2048 < /dev/urandom | base64)" '{"file_id": $file_id, "content": {"data": $data}}')

  # Use curl to send the JSON data to the /store endpoint
  curl -s -X POST "$STORE_URL" \
       -H "Content-Type: application/json" \
       -d "$JSON_DATA" > /dev/null

  echo "Data sent for file_id: $file_id"
}

# Simulate 100 users sending data concurrently
for i in $(seq 1 10000); do
  # Generate a unique file_id for each request
  file_id="example${i}"
  
  # Call send_data function in the background for concurrency
  send_data "$file_id" &
done

# Wait for all background jobs to finish
wait

echo "All data sent."
