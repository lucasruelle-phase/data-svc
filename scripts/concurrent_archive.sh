#!/bin/bash

# Base URL for the archive endpoint
ARCHIVE_URL="http://localhost:8080/archive"

# Function to archive data for a given file_id
archive_data() {
  local file_id="$1"
  
  # Use curl to send a POST request for archiving
  response=$(curl -s -o /dev/null -w "%{http_code}" -X POST "${ARCHIVE_URL}/${file_id}")
  
  if [ "$response" == "200" ]; then
    echo "Data archived for file_id: $file_id"
  else
    echo "Failed to archive data for file_id: $file_id. HTTP status: $response"
  fi
}

# Iterate over a predefined set of file_ids to simulate concurrent archiving
for i in $(seq 1 10000); do
  file_id="example${i}"
  
  # Call the archive_data function in the background for concurrency
  archive_data "$file_id" &
done

# Wait for all background jobs to finish
wait

echo "Concurrent archive requests completed."
