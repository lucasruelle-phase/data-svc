#!/bin/bash

# Define the endpoint URL
STORE_URL="http://localhost:8080/store"

# Calculate the approximate size needed for the content, accounting for JSON overhead
# This is a rough approximation; adjust as needed
required_bytes=$((2000 - 200)) # Adjusting for JSON structure overhead

# Generate a base64 string that, when included in the JSON, will result in approximately 2KB
# Note: Base64 increases the size by 33%, so we reduce the target size accordingly
base64_content=$(head -c $((required_bytes * 3 / 4)) /dev/urandom | base64 | tr -d '\n')

# Generate the JSON data
JSON_DATA=$(jq -n --arg str "$base64_content" '{
  "file_id": "example1",
  "content": {
    "nested": {
      "more_nested": {
        "data": $str
      }
    }
  }
}')

# Use curl to send the JSON data to the /store endpoint
curl -X POST "$STORE_URL" \
     -H "Content-Type: application/json" \
     -d "$JSON_DATA"

echo "Data sent to the store endpoint."
