#!/bin/bash

# Extract file_id from the first script argument
FILE_ID="example1"

# Send the archive request
curl -X POST http://localhost:8080/archive/$FILE_ID

echo "Archive request sent for file_id: $FILE_ID"
