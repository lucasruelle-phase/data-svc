#!/bin/bash

# Define the endpoint URL pattern
GET_URL="http://localhost:8080/file/%s"

# File ID to retrieve
FILE_ID="example1"

# Form the complete URL
URL=$(printf "$GET_URL" "$FILE_ID")

# Use curl to retrieve the data
curl "$URL"

echo "Request sent to retrieve data for file_id: $FILE_ID."
