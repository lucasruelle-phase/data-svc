use actix_web::{web, App, HttpResponse, HttpServer, Responder, middleware::Logger};
use serde::{Deserialize, Serialize};
use redis::{AsyncCommands, Client};
use rusoto_core::{credential::StaticProvider, HttpClient, Region};
use rusoto_s3::{PutObjectRequest, S3Client, S3};
use std::env;
use log::{info, warn, error};
use snap::write::FrameEncoder; // Add this import at the top
use std::io::Write; // Import the Write trait
use snap::read::FrameDecoder; // Add this import at the top
use tokio::io::AsyncReadExt; // Make sure you have this for reading from the decoder

#[derive(Serialize, Deserialize)]
struct FileData {
    file_id: String,
    content: serde_json::Value,
}

//async fn store_in_redis(redis_client: web::Data<Client>, data: web::Json<FileData>) -> impl Responder {
//    let mut con = redis_client.get_async_connection().await.unwrap();
//    let _: () = con.set(&data.file_id, serde_json::to_string(&data.content).unwrap()).await.unwrap();
//    HttpResponse::Ok().body("Data stored in Redis")
//}

async fn store_in_redis(redis_client: web::Data<Client>, data: web::Json<FileData>) -> impl Responder {
    let mut con = redis_client.get_async_connection().await.unwrap();

    let content = serde_json::to_string(&data.content).unwrap();
    let should_compress = env::var("ENABLE_COMPRESSION").unwrap_or_else(|_| "false".to_string()) == "true";

    let final_content = if should_compress {
        let mut encoder = FrameEncoder::new(Vec::new());
        encoder.write_all(content.as_bytes()).unwrap(); // Compress the content
        encoder.into_inner().unwrap() // Retrieve the compressed bytes
    } else {
        content.into_bytes() // Use the original content if compression is not enabled
    };

    let _: () = con.set(&data.file_id, final_content).await.unwrap();
    HttpResponse::Ok().body("Data stored in Redis")
}

async fn archive_to_s3(
    redis_client: web::Data<Client>,
    s3_client: web::Data<S3Client>,
    file_id: web::Path<String>,
) -> impl Responder {
    info!("Attempting to archive file_id: {}", file_id);
    let mut con = redis_client.get_async_connection().await.unwrap();

    // Fetching the content as Vec<u8> instead of String
    match con.get::<_, Option<Vec<u8>>>(&*file_id).await {
        Ok(Some(content)) => {
            info!("Found content for file_id: {}, proceeding to archive.", file_id);
            // Content found in Redis, proceed to archive it to S3
            let put_request = PutObjectRequest {
                bucket: "test".to_string(),
                key: file_id.clone(),
                body: Some(content.into()), // Directly using Vec<u8> as body
                ..Default::default()
            };

            match s3_client.put_object(put_request).await {
                Ok(_) => {
                    info!("Successfully archived file_id: {}", file_id);
                    let _: () = con.del(&*file_id).await.unwrap(); // Optionally, delete from Redis after successful archive
                    HttpResponse::Ok().body("Data archived to S3")
                },
                Err(e) => {
                    error!("Failed to archive file_id: {}. Error: {:?}", file_id, e);
                    HttpResponse::InternalServerError().body("Failed to archive data")
                },
            }
        },
        Ok(None) => {
            HttpResponse::NotFound().body("Data not found in Redis")
        },
        Err(_) => {
            HttpResponse::InternalServerError().body("Error fetching data from Redis")
        },
    }
}

async fn get_file(
    redis_client: web::Data<Client>,
    s3_client: web::Data<S3Client>,
    file_id: web::Path<String>,
) -> impl Responder {
    let mut con = redis_client.get_async_connection().await.unwrap();
    let redis_result: Option<Vec<u8>> = con.get(&*file_id).await.unwrap();

    let should_decompress = env::var("ENABLE_COMPRESSION").unwrap_or_else(|_| "false".to_string()) == "true";

    match redis_result {
        Some(content) => {
            let final_content = if should_decompress {
                let mut decoder = FrameDecoder::new(&content[..]);
                let mut decompressed_content = Vec::new();
                std::io::Read::read_to_end(&mut decoder, &mut decompressed_content).unwrap();
                String::from_utf8(decompressed_content).expect("Found invalid UTF-8")
            } else {
                String::from_utf8(content).expect("Found invalid UTF-8")
            };

            HttpResponse::Ok().content_type("application/json").body(final_content)
        },
	None => {
            // Attempt to retrieve the file from MinIO
            let get_req = rusoto_s3::GetObjectRequest {
                bucket: "test".to_string(),
                key: file_id.to_string(),
                ..Default::default()
            };

            match s3_client.get_object(get_req).await {
                Ok(result) => {
                    let body = result.body.expect("File body is missing");
                    let mut bytes_stream = body.into_async_read();

                    let mut bytes = Vec::new();
                    tokio::io::AsyncReadExt::read_to_end(&mut bytes_stream, &mut bytes).await.unwrap();

                    let content = String::from_utf8(bytes).expect("Found invalid UTF-8");

                    // Re-cache the data in Redis
                    let _: () = con.set(&*file_id, &content).await.unwrap();

                    HttpResponse::Ok().content_type("application/json").body(content)
                },
                Err(_) => HttpResponse::NotFound().body("File not found in Redis or S3"),
            }
        }
    }
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let redis_url = env::var("REDIS_URL").unwrap_or_else(|_| "redis://redis:6379".to_string());
    let redis_client = Client::open(redis_url).unwrap();

    // Configuration for connecting to MinIO instead of AWS S3
    let s3_access_key = env::var("MINIO_ROOT_USER").unwrap_or_else(|_| "minioadmin".to_string());
    let s3_secret_key = env::var("MINIO_ROOT_PASSWORD").unwrap_or_else(|_| "minioadmin".to_string());
    let region = Region::Custom {
        name: "us-east-1".to_owned(),
        endpoint: "http://minio:9000".to_owned(),
    };
    let s3_client = S3Client::new_with(
        HttpClient::new().expect("Failed to create HTTP client for S3"),
        StaticProvider::new_minimal(s3_access_key, s3_secret_key),
        region,
    );

    env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default()) // Add this line to wrap your app in the logger middleware
            .app_data(web::Data::new(redis_client.clone()))
            .app_data(web::Data::new(s3_client.clone()))
            .route("/store", web::post().to(store_in_redis))
            .route("/archive/{file_id}", web::post().to(archive_to_s3))
            .route("/file/{file_id}", web::get().to(get_file))
            // Route definitions remain unchanged
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
