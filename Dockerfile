# Use a Rust base image
FROM rust:latest

# Create a work directory
WORKDIR /app

# Install cargo watch to be usable
RUN cargo install cargo-watch

# Copy the project files into the container
COPY Cargo.* .

# Install dependencies
RUN cargo build

# Set the entry command to start watchexec
CMD ["cargo", "watch", "-x", "run"]
